file_path = "football.dat"
data = {}
i = 0
with open(file_path,'rt')as f:
    for line in f:
        a = line.split()
        if a !=[] and len(a)>1 and i>0:
            F = a[6]
            A = a[8]
            if F.isnumeric() and A.isnumeric():
                data[a[1].replace(".","")] = abs(int(F)- int(A))
        i = i+1
key_min = min(data.keys(), key=(lambda k: data[k]))
print("Team" ,key_min,"," "value:", data[key_min])