
file_path = "weather.dat"

data = {}
with open(file_path,'rt')as f:
    for line in f:
        a = line.split()
        if a !=[]:
            mn = a[2].replace("*", "")
            mx = a[1].replace("*", "")
            if mn.isnumeric() and mx.isnumeric():
                data[a[0]] = int(mx)- int(mn)

key_min = min(data.keys(), key=(lambda k: data[k]))
print("id:",key_min, "value:",data[key_min])

